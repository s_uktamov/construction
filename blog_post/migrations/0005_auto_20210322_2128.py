# Generated by Django 3.1.4 on 2021-03-22 16:28

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog_post', '0004_auto_20210322_1652'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='date',
            field=models.DateField(blank=True, default=datetime.datetime(2021, 3, 22, 21, 28, 24, 2907)),
        ),
    ]
