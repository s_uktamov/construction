# Generated by Django 3.1.4 on 2021-03-22 16:28

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sotuvapp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='elon',
            name='sana',
            field=models.DateField(blank=True, default=datetime.datetime(2021, 3, 22, 21, 28, 23, 975356)),
        ),
        migrations.AlterField(
            model_name='userdetail',
            name='facebook',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='userdetail',
            name='instagram',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='userdetail',
            name='telegram',
            field=models.CharField(default='@', max_length=20),
        ),
        migrations.AlterField(
            model_name='userdetail',
            name='tiktok',
            field=models.URLField(blank=True, null=True),
        ),
    ]
